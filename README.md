# OpenML dataset: Netflix-10-Year-Stock-Data-2002-2020

https://www.openml.org/d/43829

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Netflix is one of the Biggest companies that is present in today's time. It comes under the popular FAANG companies and a dream company for many. Everyone  loves watch to watch movies and TV-Series on Netflix but the subscription prices are a little too much in my opinion. So why not invest in the company and let your stocks pay for it!
Content
This data set has 7 columns with all the necessary values such as opening price of the stock, the closing price of it, its highest in the day and much more. It has date wise data of the stock starting from 23-May-2002 to 3-Aug-2020.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43829) of an [OpenML dataset](https://www.openml.org/d/43829). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43829/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43829/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43829/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

